import { _Promise, _cb } from "./etc.js";
export async function all(promises: _Promise) {
    const results = [];
    for (const [index, promise] of Object.entries(promises)) {
        results.push(await promise);
    }
    return results;
}
