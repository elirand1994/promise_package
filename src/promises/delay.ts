export const delay = (ms: number) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
};

export const delayMessage =(ms : number, msg : string) =>{
     return delay(ms).then(()=>{
        return msg;
    })
}
