import { _Promise, _cb } from "./etc.js";

export async function each(iterable: _Promise, cb: _cb) {
    for (const [index, promise] of Object.entries(iterable)) {
        await cb(promise, parseInt(index), Object.keys(iterable).length);
    }
    return iterable;
}
