export type _Promise = Iterable<any> | Promise<Iterable<any>>;
export type _cb = (
    value: any,
    index?: number,
    arrayLength?: number
) => Promise<any>;
export type ReduceCallback = (
    accumulator: any,
    item: any,
    index?: number,
    length?: number
) => Promise<any>;
export interface obj {
    [key: string]: Promise<unknown>;
}
