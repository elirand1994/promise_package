import { _Promise, _cb } from "./etc.js";
export async function filterSeries(iterable: _Promise, cb: _cb) {
    const results = [];
    for (const [index, promise] of Object.entries(iterable)) {
        if (await cb(promise, parseInt(index), Object.keys(iterable).length)) results.push(promise);
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------

export async function filterParallel(iterable: Iterable<unknown>, cb: _cb) {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));

    for (const [i, p] of pending.entries()) {
        const res = await p;
        results.push(res);
    }

    return typeof iterable === "string" ? results.join("") : results;
}
