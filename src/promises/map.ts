import { _Promise, _cb } from "./etc.js";
export async function mapParallel(iterable: Iterable<unknown>, cb: _cb) {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));
    for (const p of pending) {
        results.push(await p);
    }
    return results;
}

//--------------------------------------------------

export async function mapSeries(iterable: Iterable<unknown>, cb: _cb) {
    const results = [];
    for (const item of iterable) {
        results.push(await cb(item));
    }
    return typeof iterable === "string" ? results.join("") : results;
}
