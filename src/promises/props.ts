import { obj } from "./etc.js";
export async function props(promisesObj: obj) {
    const results: any = {};
    for (const key in promisesObj) {
        results[key] = await promisesObj[key];
    }
    return results;
}
