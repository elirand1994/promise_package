export async function race(promises: Array<unknown>) {
    return await new Promise((resolve) => {
        promises.forEach(async (p: unknown) => {
            resolve(await p);
        });
    });
}

export async function some(promises: Array<unknown>, num: number) {
    const results: any = [];
    return await new Promise((resolve) => {
        promises.forEach(async (p: unknown) => {
            results.push(await p);
            if (results.length === num) resolve(results);
        });
    });
}
