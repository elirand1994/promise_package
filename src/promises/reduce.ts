import { ReduceCallback } from "./etc.js";

export async function reduce(
    iterable: Iterable<unknown>,
    cb: ReduceCallback,
    initial: unknown
) {
    const iter = Array.from(iterable);
    let aggregator = initial || iter[0];

    for (const item of iterable) {
        aggregator = await cb(aggregator, item);
    }

    return aggregator;
}
