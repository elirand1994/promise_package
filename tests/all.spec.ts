import {all} from "../src/promises/all"
import {delayMessage} from "../src/promises/delay";
import { expect } from "chai";

describe("Testing promises API", ()=>{
    context("Testing all function",()=>{
        it("Should evalue each given value and return it after 500ms, for each promise",async ()=>{
            const promise1 = delayMessage(500,"hi delayed");
            const promise2 = delayMessage(500,"bye delayed");
            await all([promise1,promise2]).then((result)=>{
                expect([result[0],result[1]]).to.be.eql(["hi delayed","bye delayed"])
            })
        })

    })
})