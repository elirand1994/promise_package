import { each } from "../src/promises/each";
import { delay } from "../src/promises/delay";
import { expect } from "chai";
describe("Testing promsies API", () => {
    context("Testing each function", () => {
        it("Should return an array from the string given with 100ms delay between each char", async () => {
            const result: string[] = [];
            await each("Geronimo", async (char) => {
                await delay(100);
                result.push(char);
            }).then(() => {
                expect(result).to.be.eql(["G","e","r","o","n","i","m","o",]);
            });
        });
    });
});
