import { filterParallel, filterSeries } from "../src/promises/filter";
import { delay } from "../src/promises/delay";
import { expect } from "chai";
describe("Testing promises API", () => {
    context("Testing filter functions", () => {
        it("Should filter only alphabetical letters serially", () => {
            const check = "Geronimo";
            filterSeries("G<4!e3ro0ni1mo", async (char) => {
                await delay(100);
            }).then((result) => {
                expect(result).to.be.eql(check);
            });
        });
    });
    context("Testing filter functions", () => {
        it("Should filter only alphabetical letters parallaly", () => {
            const check = "Geronimo";
            filterParallel("G<4!e3ro0ni1mo", async (char) => {
                await delay(100);
            }).then((result) => {
                expect(result).to.be.eql(check);
            });
        });
    });
});
