import {mapParallel,mapSeries} from "../src/promises/map"
import {delay} from "../src/promises/delay";
import { expect } from "chai";
describe("Testing promises API",()=>{
    context("Testing map parallel",()=>{
        it("Should map each char parallely to a capital letter",()=>{
            const check = "GERONIMO";
            mapParallel('Geronimo', async char => {
                await delay(100);
                return char.toUpperCase();
            }).then((result)=>{
                expect(result).to.be.eql(check)
            })
        })
    })
    context("Testing map series",()=>{
        it("Should map each char serially to a capital letter",()=>{
            const check = "GERONIMO";
            mapSeries('Geronimo', async char => {
                await delay(100);
                return char.toUpperCase();
            }).then((result)=>{
                expect(result).to.be.eql(check)
            })
        })
    })
})