import {props} from "../src/promises/props"
import {delayMessage} from "../src/promises/delay";
import { expect } from "chai";
const promise1 = delayMessage(300,"hi");
const promise2 = delayMessage(300, "bye");
describe("Testing promises API",()=>{
    context("Testing props function", ()=>{
        it("Should resolve each promise and then return as an object",async ()=>{
            await props({promise1,promise2}).then(result=>{
                expect(result).to.haveOwnProperty("promise1").eql("hi");
                expect(result).to.haveOwnProperty("promise2").eql("bye");
            })
        })
        
    })
})