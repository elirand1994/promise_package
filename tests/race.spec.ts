import { race } from "../src/promises/race";
import { delayMessage } from "../src/promises/delay";
import { expect } from "chai";
describe("Testing promises API", () => {
    context("Testing race function", () => {
        it("Should return the 2nd promise resolved value, as it is the fastest", async () => {
            race([
                delayMessage(1000, "first"),
                delayMessage(500 ,"second"),
                delayMessage(1500, "third"),
            ]).then((result)=>{
                expect(result).to.be.equal("second");
            });
        });
    });
});
