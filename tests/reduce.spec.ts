import { reduce } from "../src/promises/reduce";
import { expect } from "chai";
describe("Testing promises API", () => {
    context("Testing reduce function", () => {
        it("Should return the reduced sum of the array", async () => {
            reduce(
                [1, 2, 3, -4, 10],
                async (total, num) => {
                    return total + num;
                },
                0
            ).then((result)=>{
                expect(result).to.be.equal(12)
                expect(typeof result).to.be('number');
            })
        });
    });
});
