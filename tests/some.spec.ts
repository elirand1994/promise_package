import { some } from "../src/promises/race";
import { delayMessage } from "../src/promises/delay";
import { expect } from "chai";
describe("Testing promises API", () => {
    context("Testing some function", () => {
        it("Should return an array of 2 promises which have been resolved", async () => {
            some([
                delayMessage(500, "first"),
                delayMessage(500 ,"second"),
                delayMessage(500, "third"),
            ],2).then((result)=>{
                expect(result).to.have.lengthOf(2);
                   
            });
        });
    });
});