import { _Promise, _cb } from "./etc.js";
export declare function each(iterable: _Promise, cb: _cb): Promise<Iterable<any>>;
