export declare type _Promise = Iterable<any> | Promise<Iterable<any>>;
export declare type _cb = (value: any, index?: number, arrayLength?: number) => Promise<any>;
export declare type ReduceCallback = (accumulator: any, item: any, index?: number, length?: number) => Promise<any>;
export interface obj {
    [key: string]: string;
}
