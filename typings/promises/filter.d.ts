import { _Promise, _cb } from "./etc.js";
export declare function filterSeries(iterable: _Promise, cb: _cb): Promise<string | any[]>;
export declare function filterParallel(iterable: Iterable<unknown>, cb: _cb): Promise<string | any[]>;
