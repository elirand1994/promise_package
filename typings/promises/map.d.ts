import { _cb } from "./etc.js";
export declare function mapParallel(iterable: Iterable<unknown>, cb: _cb): Promise<any[]>;
export declare function mapSeries(iterable: Iterable<unknown>, cb: _cb): Promise<string | any[]>;
