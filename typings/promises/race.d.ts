export declare function race(promises: Array<unknown>): Promise<unknown>;
export declare function some(promises: Array<unknown>, num: number): Promise<unknown>;
