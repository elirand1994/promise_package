import { ReduceCallback } from "./etc.js";
export declare function reduce(iterable: Iterable<unknown>, cb: ReduceCallback, initial: unknown): Promise<unknown>;
